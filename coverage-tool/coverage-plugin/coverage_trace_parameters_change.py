# !/usr/bin/env python
##############################################################################
# Copyright (c) 2022, ARM Limited and Contributors. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
##############################################################################
import iris.debug as debug
import iris.iris as iris
import argparse
import textwrap

def arg_parser():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
        description  = textwrap.dedent('''\
                Tool to change the plugin parameters of the FVP coverage-plugin.

                To be able to connect to the model, the model needs to have the iris-servier active.
                The -p of the model can be used to show the port to connect to.
                When starting a model with the iris-server, the model will wait until a debugger
                is connect and ran. We can start the model automatically by using the -R option.
                Example:
                MODEL --iris-server -R -p
            '''))
    parser.add_argument(
            "-p",
            "--port",
            type=int,
            default=7100,
            help="set the port of the running model"
            )
    parser.add_argument(
            "-m",
            "--mode",
            type=lambda x: int(x,0),
            default=-1,
            help="set the mode to trace as a hex number."
            )
    parser.add_argument(
            "-f",
            "--file",
            default="",
            help="set the file-prefix of the trace files"
            )
    parser.add_argument(
            "-s",
            "--save",
            action='store_true',
            help="Save the current trace"
            )
    return parser.parse_args()

def main():
    parsed_args = arg_parser()
    model = debug.NetworkModel("localhost", parsed_args.port)

    components = model.get_targets()
    for component in components:
        if "coverage_trace" in component.instName:
            trace_component = component

    if trace_component:
        if parsed_args.mode != -1:
            trace_component.parameters['trace-mode'] = parsed_args.mode
            print("Changed trace mode to " + hex(parsed_args.mode))
        elif parsed_args.file != "":
            trace_component.parameters['trace-file-prefix'] = parsed_args.file
            print("Changed trace prefix to" +  parsed_args.file)
        elif parsed_args.save:
            trace_component.parameters['trace-file-prefix'] = trace_component.parameters['trace-file-prefix']
            print("Saved trace files to " + trace_component.parameters['trace-file-prefix'])
        else:
            print("Nothing to do")
    else:
        print("Could not find plugin component")


if __name__ == "__main__":
    main()
