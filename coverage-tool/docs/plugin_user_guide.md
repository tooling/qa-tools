# coverage-plugin User Guide

The *coverage-plugin* is a C++ project using the Model Trace Interface Plugin Development Kit (MTIPDK) in order to create a trace plugin, which is a special shared library. The trace plugins can be loaded into Arm Fast Models to produce execution trace data for doing code coverage measurement.

## Dependencies
- GCC 7.5.0 at least

## Building the coverage-plugin
```bash
$ cd coverage-plugin
$ make PVLIB_HOME=</path/to/model_library>
```

## Capturing a trace

You need to add two options to your model command-line:

```bash
   --plugin /path/to/coverage_trace.so
   -C TRACE.coverage_trace.trace-file-prefix="/path/to/TRACE-PREFIX"
```

You can then run your FVP model. The traces will be created at the end of the simulation*.

BEWARE: Traces aren't numbered and will be overwritten if you do two successive runs. Aggregating results will require moving traces to a separate place or changing the prefix between runs. This is the responsibility of the plugin user.

*NOTE: The plugin captures the traces in memory and on the termination of the simulation it writes the data to a file. If user terminates the simulation forcefully with a Ctrl+C the trace files are not generated. Traces can be saved by interacting with the plugin itself. More info can be found in the [Interacting with the plugin chapter](#interacting-with-the-plugin) .

### Exception level based tracing.

The coverage tool support only tracing specific exception levels. This is useful when trying to trace a small
part of a bigger system. This is for example used to measure the code coverage of the OP-TEE secure OS.
Where the trace is only being recorded while being inside S-EL1. The exception level can be
specified by passing the `TRACE.coverage_trace.trace-mode` option. The passed value is a 32b bit field.

|exception Level |    Bit Field     |
|----------------|------------------|
| **None secure modes**             |
|EL0t            |    0             |
|EL1t            |    1             |
|EL1h            |    2             |
|EL2t            |    3             |
|EL2h            |    4             |
|EL3t            |    5             |
|EL3h            |    6             |
|usr             |    7             |
|fiq             |    8             |
|irq             |    9             |
|svc             |    10            |
|mon             |    11            |
|abt             |    12            |
|hyp             |    13            |
|und             |    14            |
|sys             |    15            |
| **Secure modes**                  |
|EL0t            |    16            |
|EL1t            |    17            |
|EL1h            |    18            |
|EL2t            |    19            |
|EL2h            |    20            |
|EL3t            |    21            |
|EL3h            |    22            |
|usr             |    23            |
|fiq             |    24            |
|irq             |    25            |
|svc             |    26            |
|mon             |    27            |
|abt             |    28            |
|hyp             |    29            |
|und             |    30            |
|sys             |    31            |

### Interacting with the plugin

The `trace-mode` and the `trace-file-prefix` can both be changed at run-time.
Every time the `trace-file-prefix` value is changed, the plugin will save the trace result.
This can be used to save the result without the need for the simulation to end.

```python
#!/usr/bin/python
import iris.debug as debug
import iris.iris as iris


trace_file_prefix = ""
self.model = debug.NetworkModel("localhost",9000)

self.trace_component = None
components = self.model.get_targets()
for component in components:
    if "coverage_trace" in component.instName:
        self.trace_component = component

if not self.trace_component:
    fail_msg("Trace component not found")
else:
    # Save the trace results
    self.trace_component.parameters['trace-file-prefix'] = trace_file_prefix
```
## License
[BSD-3-Clause](../../license.md)
